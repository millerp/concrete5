<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2015-03-24T16:09:35-03:00
 *
 * @item      misc.latest_version
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'site' => 'Demo',
    'version_installed' => '5.7.3.1',
    'locale' => 'pt_BR',
    'misc' => array(
        'access_entity_updated' => 1427218102,
        'latest_version' => '5.7.3.1',
        'seen_introduction' => true,
        'do_page_reindex_check' => false
    ),
    'i18n' => array(
        'choose_language_login' => '1'
    ),
    'cache' => array(
        'blocks' => false,
        'assets' => false,
        'theme_css' => false,
        'overrides' => false,
        'pages' => '0',
        'full_page_lifetime' => 'default',
        'full_page_lifetime_value' => null
    ),
    'theme' => array(
        'compress_preprocessor_output' => false
    ),
    'debug' => array(
        'detail' => 'debug',
        'display_errors' => true
    )
);
